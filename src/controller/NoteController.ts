import { Request, Response } from 'express';
import { Note } from '../model/Note';
import { NoteRepo } from '../repository/NoteRepo';

class NoteController {
    async create(req: Request, res: Response) {
        try {
            const newNote = new Note();
            newNote.name = req.body.name;
            newNote.description = req.body.description;

            await new NoteRepo().save(newNote);

            res.status(201).json({
                status: "Created",
                message: "Successfully created note!",
            });

        } catch (error) {
            res.status(500).json({
                status: "Internal Server Error!",
                message: "Internal Server Error!",
            });
        }
    }

    async update(req: Request, res: Response) {
        try {
            const id = parseInt(req.params?.id);
            const newNote = await new Note();
            
            newNote.id = id;
            newNote.name = req.body.name;
            newNote.description = req.body.description;

            await new NoteRepo().update(newNote);

            res.status(200).json({
                status: "OK",
                message: "Successfully updated note!",
                data: newNote
            });

        } catch (error) {
            res.status(500).json({
                status: "Internal Server Error!",
                message: "Internal Server Error!",
            });
        }
    }

    async delete(req: Request, res: Response) {
        try {
            let id = parseInt(req.params["id"]);
            await new NoteRepo().delete(id);

            res.status(200).json({
                status: "OK",
                message: "Successfully deleted note!",
            });

        } catch (error) {
            res.status(500).json({
                status: "Internal Server Error!",
                message: "Internal Server Error!",
            });
        }
    }

    async findAll(req: Request, res: Response) {
        try {
            const newNotes = await new NoteRepo().retrieveAll();

            res.status(200).json({
                status: "OK",
                message: "Successfully retrieved notes!",
                data: newNotes
            });

        } catch (error) {
            res.status(500).json({
                status: "Internal Server Error!",
                message: "Internal Server Error!",
            });
        }
    }

    async findById(req: Request, res: Response) {
        try {
            const newNote = await new NoteRepo().retrieveById(parseInt(req.params?.id));

            res.status(200).json({
                status: "OK",
                message: "Successfully retrieved note!",
                data: newNote
            });

        } catch (error) {
            res.status(500).json({
                status: "Internal Server Error!",
                message: "Internal Server Error!",
            });
        }
    }
}

export default new NoteController();