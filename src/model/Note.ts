import { Model, DataTypes } from '@sequelize/core';
import { Table, Attribute, PrimaryKey, AutoIncrement, NotNull } from '@sequelize/core/decorators-legacy';

@Table({
    tableName: Note.NOTE_TABLE_NAME
})

export class Note extends Model {

    public static NOTE_TABLE_NAME = 'note' as string;

    @Attribute(DataTypes.INTEGER)
    @PrimaryKey
    @NotNull
    @AutoIncrement
    declare id: number;

    @Attribute(DataTypes.STRING(100))
    @NotNull
    declare name: string;

    @Attribute(DataTypes.STRING(255))
    @NotNull
    declare description: string;
}