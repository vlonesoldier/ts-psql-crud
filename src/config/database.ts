import { Sequelize } from '@sequelize/core';
import * as dotenv from 'dotenv';
import { Note } from '../model/Note';
dotenv.config();


class Database {
    public sequelize: Sequelize | undefined;

    private POSTGRES_HOST = process.env.POSTGRES_HOST as string;
    private POSTGRES_PORT = process.env.POSTGRES_PORT as unknown as number;
    private POSTGRES_USER = process.env.POSTGRES_USER as string;
    private POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD as string;
    private POSTGRES_DB = process.env.POSTGRES_DB as string;

    constructor() {
        this.connectToDB();
    }

    private async connectToDB() {
        this.sequelize = new Sequelize({
            host: this.POSTGRES_HOST,
            port: this.POSTGRES_PORT,
            username: this.POSTGRES_USER,
            password: this.POSTGRES_PASSWORD,
            database: this.POSTGRES_DB,
            dialect: 'postgres',
            models: [Note]
        });
        
        await this.sequelize.authenticate().then(() => {
            console.log("✅ DB connection established")
        }).catch((err) => {
            console.log("❌ DB connection not established", err)
        })
    }
}

export default Database;